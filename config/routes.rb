Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :v1 do
    get '/battles/leaderboard', to: 'battles#leaderboard'
    resources :battles, only: [:index, :show, :create]
    get '/battles/status', to: 'battles#status'
    get '/battles/health', to: 'battles#health'
  end

end
