class V1::BattlesController < ApplicationController
  before_action :validate_request

  def health
    render json: { message: 'Arena API functioning'}, status: 200
  end

  # Accepts a JSON body
  # { battlepets_ids: [], type: 'strenght|speed' }
  def create
    render json: { message: 'You must have an array of size 2 UUIDs'}, status: :bad_request and return if
        params[:battlepets_ids].blank? or
            params[:battlepets_ids].length != 2
    render json: { message: 'You must specify \'type\''}, status: :bad_request if params[:type].blank?
    wunder_api = Wunder::Battlepets::Api::V1.new
    pet_1 = wunder_api.get_pet(params[:battlepets_ids][0]).symbolize_keys
    pet_2 = wunder_api.get_pet(params[:battlepets_ids][1]).symbolize_keys
    render json: { message: 'One of the pets are not available. Can not create a battle'}, status: :not_found and return if
        pet_1.blank? or pet_2.blank?
    # Begin a battle
    battle_type = params[:type].to_sym
    battle = Battle.new(pet_1: pet_1, pet_2: pet_2, status: :finished, battle_type: battle_type)
    arena = Arenas::Arena.new(pet_1, pet_2, battle_type)
    battle_log = arena.fight
    battle.battle_log = battle_log
    battle.winner = battle_log.last[:winner]
    battle.save!

    render json: { battle: { id: battle.id, status: battle.status }}, status: :ok
  end

  def show
    begin
      battle = Battle.find(params[:id])
      render json: { id: battle.id, status: battle.status, created_at: battle.created_at } and return if battle.queued?
      render json: battle, status: :ok
    rescue ActiveRecord::RecordNotFound => e
      render json: { message: 'Battle not found' }, status: :not_found if battle.blank?
    end
  end

  def leaderboard
    # Battle.all.group(:winner).order(:count).count.to_a
    render json: { leaderboard: Battle.all.select('battles.winner, count(1) as wins').group('battles.winner').order('wins desc') }, status: :ok
  end


    private

    def validate_request
      #a request is valid if it has a valid token set in X-Pets-Arena-Token
      render status: 401 if ENV['HTTP_X_PETS_ARENA_TOKEN'].blank? or
          ENV['HTTP_X_PETS_ARENA_TOKEN'] != ENV['ARENA_AUTHORIZATION_TOKEN'] if ENV['USE_AUTH_TOKEN'].present?
    end
end