class Battle < ApplicationRecord
  # store :battle_log,
  #       accesors: [:pet_1_hitpoints, :pet_2_hitpoints, :damage_done, :damage_by, :winner], coder: JSON
  # store :pet_1, accessors: [:id, :name, :strength, :intelligence, :speed, :integrity], coder: JSON
  # store :pet_2, accessors: [:id, :name, :strength, :intelligence, :speed, :integrity], coder: JSON
  serialize :battle_log
  serialize :pet_1
  serialize :pet_2
  enum status: [:queued, :error, :finished]
  enum battle_type: [:strength,:speed]

  validates_presence_of :pet_1, :pet_2
end
