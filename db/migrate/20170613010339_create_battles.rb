class CreateBattles < ActiveRecord::Migration[5.1]
  def change
    create_table :battles, id: :uuid do |t|
      t.text :pet_1, null: false
      t.text :pet_2, null: false
      t.text :battle_log
      t.integer :battle_type, default: 0
      t.uuid :winner
      t.string :message
      t.integer :status, default: 0

      t.timestamps
    end

    add_index :battles, :pet_1
    add_index :battles, :pet_2
    add_index :battles, :winner

  end
end
