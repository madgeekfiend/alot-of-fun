class Arenas::Arena

  def initialize(pet_1, pet_2, attribute = :strength, maximum_damage = 10, starting_hitpoints = 50)
    @pet_1 = pet_1
    @pet_2 = pet_2
    @maximum_damage = maximum_damage
    @starting_hitpoints = starting_hitpoints
    @fighting_attribute = attribute
  end

  def fight
    @pet_1.merge! ({'hitpoints': @starting_hitpoints })
    @pet_2.merge! ({'hitpoints': @starting_hitpoints})
    players_turn = 0
    do_attacks @pet_1, @pet_2, @fighting_attribute,
               players_turn, @maximum_damage
  end

  def do_attacks(pet_1, pet_2, attribute, starting_player, max_damage_potential)

    attacks = []
    players_turn = starting_player
    # use the same attack pattern for all attributes
    while pet_1[:hitpoints] > 0 and pet_2[:hitpoints] > 0 do
      # pet 1 always goes first perhaps have them compete with speed in the future
      # for initiative
      if players_turn == 0
        # player 1 turn
        die_roll = rand(100)+1
        if die_roll > pet_2[attribute]
          # player 1 hit calculate damage and subtract
          damage_done = rand(max_damage_potential) + 1
          pet_2[:hitpoints] -= damage_done
          result =  { pet_1_hitpoints: @pet_1[:hitpoints], pet_2_hitpoints: pet_2[:hitpoints],
                      damage_done: damage_done, damage_by: 1}
          if pet_2[:hitpoints] < 1 # winner is pet 1
            result.merge!({winner:pet_1[:id]})
            attacks << result
            break
          end
          attacks << result
        end
        players_turn = 1
      else
        # player 2 turn and always assume only 2 participants
        die_roll = rand(100)+1
        if die_roll > pet_1[attribute]
          # player 1 hit calculate damage and subtract
          damage_done = rand(max_damage_potential) + 1
          pet_1[:hitpoints] -= damage_done
          result =  { pet_1_hitpoints: pet_1[:hitpoints], pet_2_hitpoints: pet_2[:hitpoints],
                      damage_done: damage_done, damage_by: 2}
          if pet_1[:hitpoints] < 1 # winner is pet 2
            result.merge!({winner:pet_2[:id]})
            attacks << result
            break
          end
          attacks << result
        end
        players_turn = 0
      end
    end
    attacks
  end

end