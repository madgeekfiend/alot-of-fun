# Isolate the API ENDPOINT for accessing the BATTLE PETS repository
module Wunder
  module Battlepets
    module Api
      class V1
        include HTTParty
        base_uri ENV['WUNDER_PETS_API_ROOT']

        def initialize
          @options = { headers: { 'X-Pets-Token': ENV['WUNDER_PETS_API_TOKEN'] }}
        end

        def get_pet(id)
          Rails.logger.info "Getting pet ID #{id}"
          self.class.get("#{ENV['WUNDER_PETS_API_GET_PETS_PATH']}/#{id}", @options)
        end

        def get_pets
          Rails.logger.info 'Getting list of all pets'
          self.class.get(ENV['WUNDER_PETS_API_GET_PETS_PATH'], @options)
        end

      end
    end
  end
end


