require 'rails_helper'

# integration test for Wunder API

RSpec.describe 'Wunder API', :type => :request do

  before(:all) do
    @wunder_api = Wunder::Battlepets::Api::V1.new
    @battle_pets = @wunder_api.get_pets
  end

  it 'should retrieve a non-empty list of battle pets' do
    expect(@battle_pets).not_to be_empty
    expect(@battle_pets.length).to be > 0
  end

  it 'retrieve first pet from /get/:id and verify attributes' do
    first_pet = @battle_pets[0]
    pet = @wunder_api.get_pet(first_pet['id'])
    expect(pet).not_to be_empty
    expect(pet).to have_key('name')
    expect(pet).to have_key('strength')
    expect(pet).to have_key('intelligence')
    expect(pet).to have_key('speed')
    expect(pet).to have_key('integrity')
  end

end