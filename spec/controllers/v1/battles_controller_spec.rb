require 'rails_helper'
require 'helpers'

RSpec.describe V1::BattlesController, type: :controller do

  RSpec.configure do |c|
    c.include Helpers
  end

  describe 'get status check' do
    context 'service endpoint is available'
    before do
      get :health
    end
    it { should respond_with(:success)}
    it 'has message' do
      expect(get_message_from_body(response.body)).to eq('Arena API functioning')
    end

  end

  describe 'create new arena via POST' do
    it 'returns a 400 if missing battle pets array' do
      post :create
      expect(response).to have_http_status(:bad_request)
      expect(get_message_from_body(response.body)).to eq('You must have an array of size 2 UUIDs')
    end

    it 'returns a 400 if missing battle arena type' do
      post :create, params: { battlepets_ids: [1,2] }
      expect(response).to have_http_status(:bad_request)
      expect(get_message_from_body(response.body)).to eq('You must specify \'type\'')
    end
  end

  describe 'get battle status' do
    before(:each) do
      Wunder::Battlepets::Api::V1.any_instance.stub(:get_pet).and_return({
         "name": "Fluffy",
         "strength": 12,
         "intelligence": 22,
         "speed": 21,
         "integrity": 66})
    end

    it 'returns 404 if no arena found for ID' do
      get :status, params: { id: '123' }
      expect(response).to have_http_status(:not_found)
    end
    it 'returns battle is queued and id, status' do
      create :queued_battle
      get :status, params: {id: 'f163ea98-8b2e-4ff8-9f15-3a0cd0d71677'}
      expect(response).to have_http_status(:success)
      payload = JSON.parse(response.body)
      expect(payload).to have_key('id')
      expect(payload).to have_key('status')
      expect(payload).to have_key('created_at')
    end
  end

end
