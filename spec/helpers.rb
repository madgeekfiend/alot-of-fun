module Helpers
  def get_message_from_body(body)
    JSON.parse(body)['message']
  end

end