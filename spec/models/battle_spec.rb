require 'rails_helper'
require 'securerandom'

RSpec.describe Battle, type: :model do
  it { should validate_presence_of(:pet_1)}
  it { should validate_presence_of(:pet_2)}
  it 'is valid with valid attributes' do
    expect(Battle.new(pet_1: { name:'Fluffy', strength: 12, intelligence: 22, speed: 21, integrity: 66 },
                      pet_2: { name:'Bob', strength: 24, intelligence: 13, speed: 77, integrity: 55 })).to be_valid
  end
  it 'is not valid without pet_1 and pet_2' do
    expect(Battle.new).to_not be_valid
  end
  it 'is not valid with pet_1 and not pet_2' do
    expect(Battle.new(pet_1: { name:'Fluffy', strength: 12, intelligence: 22, speed: 21, integrity: 66 })).to_not be_valid
  end
  it 'is not valid without pet_1 and with pet_2' do
    expect(Battle.new(pet_2: { name:'Fluffy', strength: 12, intelligence: 22, speed: 21, integrity: 66 })).to_not be_valid
  end
end
