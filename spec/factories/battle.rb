require 'securerandom'

FactoryGirl.define do
  factory :battle do
    pet_1 { { name:'Fluffy', strength: 12, intelligence: 22, speed: 21, integrity: 66 } }
    pet_2 { { name:'Bob', strength: 24, intelligence: 13, speed: 77, integrity: 55 } }

    factory :queued_battle do
      id 'f163ea98-8b2e-4ff8-9f15-3a0cd0d71677'
      status :queued
    end

  end
end